<?php

namespace Drupal\dazzle_translation_ui\Tests\Functional;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;

/**
 * Class ConfigTranslationTest.
 *
 * @group dazzle_translation_ui
 */
class ConfigTranslationTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'language',
    'block',
    'dazzle_translation_ui',
    'config_translation',
    'views',
    'views_ui',
  ];

  /**
   * The url of the translation page.
   *
   * @var string
   */
  protected $translationUrl = 'admin/config/regional/translation-ui';

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    // Create a new language (Afrikaans) to use as test for translations.
    ConfigurableLanguage::createFromLangcode('af')->save();

    // Create a user with all required permissions.
    $permissions = [
      'access translation interface',
      'translate configuration',
      'translate interface',
      'administer views',
      'access files overview',
    ];
    $this->drupalLogin($this->drupalCreateUser($permissions));

    // Set the site name to something readable.
    \Drupal::configFactory()
      ->getEditable('system.site')
      ->set('name', 'Owl')
      ->save();

    // Place branding block.
    $this->drupalPlaceBlock('system_branding_block', ['region' => 'header', 'id' => 'site-branding']);

    // Visit the pages to init translations.
    $this->drupalGet('<front>');
    $this->drupalGet('/af');
  }

  /**
   * Test permissions.
   */
  public function testPermissions() {
    $this->drupalGet($this->translationUrl);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogout();
    $this->drupalGet($this->translationUrl);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests that the config translation works with the site name.
   */
  public function testSiteName() {
    // Check that we can see owl on both front pages.
    $this->drupalGet('<front>');
    $this->assertSession()->pageTextContains('Owl');
    $this->drupalGet('/af');
    $this->assertSession()->pageTextContains('Owl');

    // Add translation trough the normal configuration translation interface.
    $this->drupalGet('admin/config/system/site-information/translate');
    $this->clickLink('Add');
    $this->drupalPostForm(NULL, ['translation[config_names][system.site][name]' => 'Llama'], 'Save translation');

    // Check that we now see the translated version.
    $this->drupalGet('<front>');
    $this->assertSession()->pageTextContains('Owl');
    $this->drupalGet('/af');
    $this->assertSession()->pageTextContains('Llama');

    // Use our own translation screen to do the translation again, to a new
    // string.
    $this->drupalGet($this->translationUrl);
    $this->assertSession()->statusCodeEquals(200);

    $field_name = 'strings[config][system.site::name][translations][0]';
    $this->assertSession()->fieldExists($field_name);
    $this->assertSession()->fieldValueEquals($field_name, 'Llama');
    $this->drupalPostForm(NULL, [$field_name => 'Uil'], 'Save translations');
    $this->assertSession()->fieldExists($field_name);
    $this->assertSession()->fieldValueEquals($field_name, 'Uil');

    // Check that we now see the updated translation.
    $this->drupalGet('<front>');
    $this->assertSession()->pageTextContains('Owl');
    $this->drupalGet('/af');
    $this->assertSession()->pageTextContains('Uil');
  }

  /**
   * Tests interface translation.
   */
  public function testInterfaceTranslation() {
    // Check that we can see the text on both front pages.
    $this->drupalGet('<front>');
    $this->assertSession()->pageTextContains('Member for');
    $this->drupalGet('/af');
    $this->assertSession()->pageTextContains('Member for');

    // Translate "Member for" to "Llama".
    $this->drupalGet($this->translationUrl);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalPostForm(NULL, ['strings[1][translations][0]' => 'Llama'], 'Save translations');

    // Check that we the translated text only shows up on the translated page.
    $this->drupalGet('<front>');
    $this->assertSession()->pageTextContains('Member for');
    $this->drupalGet('/af');
    $this->assertSession()->pageTextContains('Llama');
    $this->assertSession()->pageTextNotContains('Member for');
  }

  /**
   * Tests the filtering
   */
  public function testFilter() {
    // Add translation trough the normal configuration translation interface.
    $this->drupalGet('admin/config/system/site-information/translate');
    $this->clickLink('Add');
    $this->drupalPostForm(NULL, ['translation[config_names][system.site][name]' => 'Home'], 'Save translation');

    // Go to our ui and filter for Home
    $this->drupalGet($this->translationUrl);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Member for');
    $this->drupalPostForm(NULL, ['edit-string' => 'Home'], 'Filter');

    // Check that the page doesn't contain "Member for" anymore.
    $this->assertSession()->pageTextNotContains('Member for');
  }

  /**
   * Tests views fulltext field.
   */
  public function testFullTextField() {
    // Add a text to the not empty block.
    $this->drupalPostForm('admin/structure/views/nojs/add-handler/files/page_1/empty', ['name[views.area]' => 'views.area'], 'Add and configure no results behavior');
    $this->drupalPostForm(NULL, ['options[content][value]' => 'Empty files'], 'Apply');
    $this->drupalPostForm(NULL, [], 'Save');

    $this->drupalGet('/af/admin/content/files');
    $this->assertText('Empty files');

    $this->drupalGet('admin/structure/views/view/files/translate');
    $this->clickLink('Add');
    $this->drupalPostForm(
      NULL,
      ['translation[config_names][views.view.files][display][default][display_options][empty][area][content][value]' => 'Empty AF'],
      'Save translation'
    );
    $this->drupalGet('/af/admin/content/files');
    $this->assertText('Empty AF');

    $this->drupalGet($this->translationUrl);
    $this->drupalPostForm(
      NULL,
      ['strings[config][views.view.files::display__default__display_options__empty__area__content][translations][0]' => 'No files AF'],
      'Save translations'
    );

    $this->drupalGet('/af/admin/content/files');
    $this->assertText('No files AF');
  }

}
