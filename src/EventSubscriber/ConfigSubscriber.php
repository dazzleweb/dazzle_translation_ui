<?php

namespace Drupal\dazzle_translation_ui\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\dazzle_translation_ui\TranslationUiRepository;
use Drupal\language\Config\LanguageConfigOverrideCrudEvent;
use Drupal\language\Config\LanguageConfigOverrideEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ConfigSubscriber.
 */
class ConfigSubscriber implements EventSubscriberInterface {

  /**
   * The repository, hides all actual queries.
   *
   * @var \Drupal\dazzle_translation_ui\TranslationUiRepository
   */
  protected $repository;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * ConfigSubscriber constructor.
   *
   * @param \Drupal\dazzle_translation_ui\TranslationUiRepository $repository
   *   The translation UI repository.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(TranslationUiRepository $repository, ConfigFactoryInterface $configFactory) {
    $this->repository = $repository;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[LanguageConfigOverrideEvents::SAVE_OVERRIDE] = ['onLanguageOverrideSave', 250];
    return $events;
  }

  /**
   * Saves language overrides in the custom table.
   *
   * @param \Drupal\language\Config\LanguageConfigOverrideCrudEvent $event
   *   The config crud event.
   */
  public function onLanguageOverrideSave(LanguageConfigOverrideCrudEvent $event) {
    // Get the actual LanguageConfigOverride object to work with.
    $configOverride = $event->getLanguageConfigOverride();

    // Get information from the config object and store them in local variables
    // for easier access.
    $langcode = $configOverride->getLangcode();
    $configName = $configOverride->getName();
    $savedData = $configOverride->get();

    // Load original config from the config factory.
    $originalConfig = $this->configFactory->get($configName);

    // Loop over all data that's being saved and insert them into the data
    // storage.
    foreach ($savedData as $name => $data) {
      $this->saveOverride($data, $name, $configName, $langcode, $originalConfig);
    }
  }

  /**
   * Recursive function to save overrides.
   *
   * @param array|string $data
   *   The data to be saved.
   * @param string $configKey
   *   The config key (+ sub part of the config).
   * @param string $configName
   *   The name of the config.
   * @param string $langcode
   *   The language code.
   * @param \Drupal\Core\Config\ImmutableConfig $originalConfig
   *   The original config object
   */
  private function saveOverride($data, $configKey, $configName, $langcode, ImmutableConfig $originalConfig) {
    // Get the original value from the config object.
    if (strpos($configKey, '__')) {
      $parts = explode('__', $configKey);
      $original_value = $originalConfig->get($parts[0]);
      unset($parts[0]);
      foreach ($parts as $part) {
        $original_value = $original_value[$part];
      }
    }
    else {
      $original_value = $originalConfig->get($configKey);
    }

    if (!is_array($data)) {
      // If the current data to be stored is simple data, we can just save it
      // and be done here.
      $this->doSave($configName, $langcode, $configKey, $original_value, $data);
    }
    else {
      foreach ($data as $key => $val) {
        $sub_key = $configKey . '__' . $key;

        // If we find a 'format' key in the array here, we need to also save the
        // format in the table; so we use that and save that into the storage, if
        // the array doesn't contain that, go deeper.
        if (isset($val['format'])) {
          $originalText = $original_value[$key]['value'];
          $this->doSave($configName, $langcode, $sub_key, $originalText, $val['value'], $val['format']);
        }
        else {
          // Recurse.
          $this->saveOverride($val, $sub_key, $configName, $langcode, $originalConfig);
        }
      }
    }
  }

  /**
   * Save to the actual data storage.
   *
   * @param string $configName
   *   The config name.
   * @param string $langcode
   *   The language code.
   * @param string $config_key
   *   The config key (+ sub part of the config)
   * @param string $originalText
   *   The original text.
   * @param string $translated
   *   The translated value.
   * @param string $format
   *   The format in which the text / translated was saved.
   */
  private function doSave($configName, $langcode, $config_key, $originalText, $translated, $format = NULL) {
    $this->repository->saveTranslationData(
      $configName,
      $langcode,
      $config_key,
      $originalText,
      $translated,
      $format
    );
  }

}
