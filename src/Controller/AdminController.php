<?php

namespace Drupal\dazzle_translation_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\dazzle_translation_ui\Form\TranslateFilterForm;
use Drupal\dazzle_translation_ui\Form\TranslationForm;

/**
 * Class AdminController.
 */
class AdminController extends ControllerBase {

  /**
   * Shows the string search screen.
   *
   * @return array
   *   The render array for the translation screen.
   */
  public function translatePage() {
    // Add a form for filtering the page + a form for the actual translation.
    return [
      'filter' => $this->formBuilder()->getForm(TranslateFilterForm::class),
      'form' => $this->formBuilder()->getForm(TranslationForm::class),
    ];
  }

}
