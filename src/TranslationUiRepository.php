<?php

namespace Drupal\dazzle_translation_ui;

use Drupal\Core\Database\Connection;

/**
 * Class TranslationUiRepository.
 */
class TranslationUiRepository {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * TranslationUiRepository constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The connection to the current database, used to run queries.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * Save data into table.
   *
   * @param string $name
   *   The config name.
   * @param string $language
   *   The lang code.
   * @param string $config_key
   *   The config key.
   * @param string $original_text
   *   The original text to display.
   * @param string $translation
   *   The new text.
   * @param string $format
   *   The text format of the text.
   */
  public function saveTranslationData($name, $language, $config_key, $original_text, $translation, $format = NULL) {
    $this->connection
      ->upsert('translation_ui')
      ->key('primary key')
      ->fields([
        'name' => $name,
        'language' => $language,
        'config_key' => $config_key,
        'original_text' => $original_text,
        'translation' => $translation,
        'format' => $format,
      ])
      ->execute();
  }

  /**
   * Load the (filtered) rows from the data set.
   *
   * @param string[] $filter_values
   *   Apply these filters.
   *
   * @return array
   *   An array of arrays that represent all the rows found in the data set with
   *   the filters applied.
   */
  public function translateFilterLoadStrings(array $filter_values = []) {
    // Language is sanitized to be one of the possible options in
    // translateFilterValues().
    $options = [
      'pager limit' => 30,
    ];

    $query = $this->connection
      ->select('translation_ui', 'ts', $options)
      ->fields('ts')
      ->condition('language', $filter_values['langcode']);

    // If we're filtering on string, we should add this into the query, however
    // we can't just use separate conditions, as we need to have the the OR
    // filter in here. When filtering for a text, it could either be the
    // original text or the already translated text that the user is search for.
    // That's why we're using ->where.
    if (!empty($filter_values['string'])) {
      $query->where('original_text = :filter OR translation = :filter', [':filter' => $filter_values['string']]);
    }

    // Execute the query to get all the results.
    $results = $query
      ->execute()
      ->fetchAll();

    // Return all items as arrays instead of stdClass objects.
    return array_map(function ($item) {
      return (array) $item;
    }, $results);
  }

  /**
   * Checks if the new translation is different from the old one.
   *
   * @param string $language
   *   The lang code.
   * @param string $name
   *   The config name.
   * @param string $config_key
   *   The config key.
   * @param string $new_text
   *   The new text.
   *
   * @return bool
   *   Returns TRUE when the text didn't exist yet or if it's different from the
   *   previous value.
   */
  public function isTranslationDifferent($language, $name, $config_key, $new_text) {
    $original_translation = $this->loadTranslation($language, $name, $config_key);
    if (count($original_translation) == 0) {
      return TRUE;
    }
    return ($original_translation[0] !== $new_text);
  }

  /**
   * Load a translation from the storage.
   *
   * @param string $language
   *   The lang code.
   * @param string $name
   *   The config name.
   * @param string $config_key
   *   The config key.
   *
   * @return array
   *   An array of translations that match the query.
   */
  protected function loadTranslation($language, $name, $config_key) {
    $results = $this->connection
      ->select('translation_ui', 'ts')
      ->fields('ts', ['translation'])
      ->condition('language', $language)
      ->condition('name', $name)
      ->condition('config_key', $config_key)
      ->execute()
      ->fetchAll();

    // Return only the translation, as it's all we need.
    return array_map(function ($item) {
      return $item->translation;
    }, $results);
  }

}
