<?php

namespace Drupal\dazzle_translation_ui\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\State\StateInterface;
use Drupal\dazzle_translation_ui\TranslationUiRepository;
use Drupal\locale\Form\TranslateFormBase;
use Drupal\locale\SourceString;
use Drupal\locale\StringStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TranslationForm.
 */
class TranslationForm extends TranslateFormBase {

  /**
   * The translation UI repository.
   *
   * @var \Drupal\dazzle_translation_ui\TranslationUiRepository
   */
  protected $translationUiRepository;

  /**
   * Constructs a new object.
   *
   * @param \Drupal\locale\StringStorageInterface $locale_storage
   *   The locale storage.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\dazzle_translation_ui\TranslationUiRepository $repository
   *   The repository.
   */
  public function __construct(StringStorageInterface $locale_storage, StateInterface $state, LanguageManagerInterface $language_manager, TranslationUiRepository $repository) {
    parent::__construct($locale_storage, $state, $language_manager);
    $this->translationUiRepository = $repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('locale.storage'),
      $container->get('state'),
      $container->get('language_manager'),
      $container->get('dazzle_translation_ui.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dazzle_translation_ui_translation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $filter_values = $this->translateFilterValues();
    $langcode = isset($filter_values['langcode']) ? $filter_values['langcode'] : NULL;

    $this->languageManager->reset();
    $languages = $this->languageManager->getLanguages();

    $langname = isset($langcode) ? $languages[$langcode]->getName() : "- None -";

    $form['#attached']['library'][] = 'locale/drupal.locale.admin';

    $form['langcode'] = array(
      '#type' => 'value',
      '#value' => $filter_values['langcode'],
    );

    $form['strings'] = array(
      '#type' => 'table',
      '#tree' => TRUE,
      '#language' => $langname,
      '#header' => [
        $this->t('Source string'),
        $this->t('Translation for @language', ['@language' => $langname]),
      ],
      '#empty' => $this->t('No strings available.'),
      '#attributes' => ['class' => ['locale-translate-edit-table']],
    );

    if (isset($langcode)) {
      $strings = $this->translateFilterLoadStrings();

      $plurals = $this->getNumberOfPlurals($langcode);

      foreach ($strings as $string) {
        // Cast into source string, will do for our purposes.
        $source = new SourceString($string);
        // Split source to work with plural values.
        $source_array = $source->getPlurals();
        $translation_array = $string->getPlurals();
        if (count($source_array) == 1) {
          // Add original string value and mark as non-plural.
          $plural = FALSE;
          $form['strings'][$string->lid]['original'] = array(
            '#type' => 'item',
            '#title' => $this->t('Source string (@language)', array('@language' => $this->t('Built-in English'))),
            '#title_display' => 'invisible',
            '#plain_text' => $source_array[0],
            '#preffix' => '<span lang="en">',
            '#suffix' => '</span>',
          );
        }
        else {
          // Add original string value and mark as plural.
          $plural = TRUE;
          $original_singular = [
            '#type' => 'item',
            '#title' => $this->t('Singular form'),
            '#plain_text' => $source_array[0],
            '#prefix' => '<span class="visually-hidden">' . $this->t('Source string (@language)', array('@language' => $this->t('Built-in English'))) . '</span><span lang="en">',
            '#suffix' => '</span>',
          ];
          $original_plural = [
            '#type' => 'item',
            '#title' => $this->t('Plural form'),
            '#plain_text' => $source_array[1],
            '#preffix' => '<span lang="en">',
            '#suffix' => '</span>',
          ];
          $form['strings'][$string->lid]['original'] = [
            $original_singular,
            ['#markup' => '<br>'],
            $original_plural,
          ];
        }
        if (!empty($string->context)) {
          $form['strings'][$string->lid]['original'][] = [
            '#type' => 'inline_template',
            '#template' => '<br><small>{{ context_title }}: <span lang="en">{{ context }}</span></small>',
            '#context' => [
              'context_title' => $this->t('In Context'),
              'context' => $string->context,
            ],
          ];
        }
        // Approximate the number of rows to use in the default textarea.
        $rows = min(ceil(str_word_count($source_array[0]) / 12), 10);
        if (!$plural) {
          $form['strings'][$string->lid]['translations'][0] = array(
            '#type' => 'textarea',
            '#title' => $this->t('Translated string (@language)', array('@language' => $langname)),
            '#title_display' => 'invisible',
            '#rows' => $rows,
            '#default_value' => $translation_array[0],
            '#attributes' => array('lang' => $langcode),
          );
        }
        else {
          // Add a textarea for each plural variant.
          for ($i = 0; $i < $plurals; $i++) {
            $form['strings'][$string->lid]['translations'][$i] = array(
              '#type' => 'textarea',
              // @todo Should use better labels https://www.drupal.org/node/2499639
              '#title' => ($i == 0 ? $this->t('Singular form') : $this->formatPlural($i, 'First plural form', '@count. plural form')),
              '#rows' => $rows,
              '#default_value' => isset($translation_array[$i]) ? $translation_array[$i] : '',
              '#attributes' => array('lang' => $langcode),
              '#prefix' => $i == 0 ? ('<span class="visually-hidden">' . $this->t('Translated string (@language)', array('@language' => $langname)) . '</span>') : '',
            );
          }
          if ($plurals == 2) {
            // Simplify interface text for the most common case.
            $form['strings'][$string->lid]['translations'][1]['#title'] = $this->t('Plural form');
          }
        }
      }

      // Add config translation strings.
      $strings = $this->translationUiRepository
        ->translateFilterLoadStrings($filter_values);

      foreach ($strings as $string) {
        // Add original string value and mark as non-plural.
        $form['strings']['config'][$string['name'] . '::' . $string['config_key']]['original_text'] = [
          '#type' => 'item',
          '#title' => $this->t('Source string (@language)', ['@language' => $this->t('Built-in English')]),
          '#title_display' => 'invisible',
          '#plain_text' => $string['original_text'],
          '#preffix' => '<span lang="en">',
          '#suffix' => '</span>',
        ];
        // Approximate the number of rows to use in the default textarea.
        $rows = min(ceil(str_word_count($string['translation']) / 12), 10);
        $form['strings']['config'][$string['name'] . '::' . $string['config_key']]['translations'][0] = array(
          '#type' => 'textarea',
          '#title' => $this->t('Translated string (@language)', array('@language' => $langname)),
          '#title_display' => 'invisible',
          '#rows' => $rows,
          '#default_value' => $string['translation'],
          '#attributes' => ['lang' => $langcode],
        );
      }

      // Add button.
      if (count(Element::children($form['strings']))) {
        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Save translations'),
        ];
      }
    }
    $form['pager']['#type'] = 'pager';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $language = $form_state->getValues()['langcode'];

    $updated = [];

    // Preload all translations for strings in the form.
    $lids = array_keys($form_state->getValue('strings'));
    $existing_translation_objects = [];

    $filter = ['lid' => $lids, 'language' => $language, 'translated' => TRUE];
    foreach ($this->localeStorage->getTranslations($filter) as $existing_translation_object) {
      $existing_translation_objects[$existing_translation_object->lid] = $existing_translation_object;
    }

    foreach ($form_state->getValue('strings') as $lid => $new_translation) {
      if ($lid === 'config') {
        continue;
      }
      $existing_translation = isset($existing_translation_objects[$lid]);

      // Plural translations are saved in a delimited string. To be able to
      // compare the new strings with the existing strings a string in the same
      // format is created.
      $new_translation_string_delimited = implode(LOCALE_PLURAL_DELIMITER, $new_translation['translations']);

      // Generate an imploded string without delimiter, to be able to run
      // empty() on it.
      $new_translation_string = implode('', $new_translation['translations']);

      $is_changed = FALSE;

      if ($existing_translation && $existing_translation_objects[$lid]->translation != $new_translation_string_delimited) {
        // If there is an existing translation in the DB and the new translation
        // is not the same as the existing one.
        $is_changed = TRUE;
      }
      elseif (!$existing_translation && !empty($new_translation_string)) {
        // Newly entered translation.
        $is_changed = TRUE;
      }

      if ($is_changed) {
        // Only update or insert if we have a value to use.
        $target = isset($existing_translation_objects[$lid]) ? $existing_translation_objects[$lid] : $this->localeStorage->createTranslation(['lid' => $lid, 'language' => $language]);
        $target->setPlurals($new_translation['translations'])
          ->setCustomized()
          ->save();
        $updated[] = $target->getId();
      }
      if (empty($new_translation_string) && isset($existing_translation_objects[$lid])) {
        // Empty new translation entered: remove existing entry from database.
        $existing_translation_objects[$lid]->delete();
        $updated[] = $lid;
      }
    }

    foreach ($form_state->getValue('strings') as $key => $translations) {
      if ($key !== 'config') {
        continue;
      }
      foreach ($translations as $k => $translation) {
        $new_text = $translation['translations'][0];
        list($name, $config_key) = explode('::', $k);

        if ($this->translationUiRepository->isTranslationDifferent($language, $name, $config_key, $new_text)) {
          // Save config.
          /** @var \Drupal\Core\Config\Config $config_translation */
          $config_translation = $this->languageManager->getLanguageConfigOverride(
            $language,
            $name
          );

          // If this not a simple config, but one with multiple layers, we need
          // to figure this out and update the updated part of the saved array.
          if (strpos($config_key, '__')) {
            $parts = explode('__', $config_key);
            $config_name = $parts[0];
            $current_config = $config_translation->get($config_name);
            $config = &$current_config;
            unset($parts[0]);
            foreach ($parts as $part) {
              $config = &$config[$part];
            }
            $config['value'] = $new_text;
            $config_translation->set($config_name, $current_config);
          }
          else {
            $config_translation->set($config_key, $new_text);
          }
          $config_translation->save();
        }
      }
    }

    if ($updated) {
      // Clear cache and force refresh of JavaScript translations.
      _locale_refresh_translations([$language], $updated);
      _locale_refresh_configuration([$language], $updated);
    }
  }

}
