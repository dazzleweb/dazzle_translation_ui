<?php

namespace Drupal\dazzle_translation_ui\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\locale\Form\TranslateFilterForm as LocaleTranslateFilterForm;

/**
 * Provides a filtered translation edit form.
 *
 * This copies 99% of the functionality from the locale translation filter form,
 * but we can't do a redirect to that page after doing the filtering, so we
 * override the ::submitForm and ::resetForm methods and set the redirects to
 * the correct route.
 */
class TranslateFilterForm extends LocaleTranslateFilterForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dazzle_translation_ui_translate_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->setRedirect('dazzle_translation_ui.translate_page');
  }

  /**
   * Provides a submit handler for the reset button.
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    parent::resetForm($form, $form_state);
    $form_state->setRedirect('dazzle_translation_ui.translate_page');
  }

}
